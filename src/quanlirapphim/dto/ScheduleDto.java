package quanlirapphim.dto;

import java.util.Date;

/**
 * @author TamAnh
 *
 */
public class ScheduleDto {
	private int idSchedule;
	private int idRoomSchedule;
	private String movieName;
	private Date startDate;
	private String dayOfWeek;
	private int priceticket;
	
	public ScheduleDto() {
		super();
	}
	public ScheduleDto(int idSchedule, int idRoomSchedule, String movieName, Date startDate, String dayOfWeek) {
		super();
		this.idSchedule = idSchedule;
		this.idRoomSchedule = idRoomSchedule;
		this.movieName = movieName;
		this.startDate = startDate;
		this.dayOfWeek = dayOfWeek;
	}
	public int getIdSchedule() {
		return idSchedule;
	}
	public void setIdSchedule(int idSchedule) {
		this.idSchedule = idSchedule;
	}
	public int getIdRoomSchedule() {
		return idRoomSchedule;
	}
	public void setIdRoomSchedule(int idRoomSchedule) {
		this.idRoomSchedule = idRoomSchedule;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getDayOfWeek() {
		return dayOfWeek;
	}
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}	
	public int getPriceticket() {
		return priceticket;
	}
	public void setPriceticket(int priceticket) {
		this.priceticket = priceticket;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dayOfWeek == null) ? 0 : dayOfWeek.hashCode());
		result = prime * result + idRoomSchedule;
		result = prime * result + idSchedule;
		result = prime * result + ((movieName == null) ? 0 : movieName.hashCode());
		result = prime * result + priceticket;
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScheduleDto other = (ScheduleDto) obj;
		if (dayOfWeek == null) {
			if (other.dayOfWeek != null)
				return false;
		} else if (!dayOfWeek.equals(other.dayOfWeek))
			return false;
		if (idRoomSchedule != other.idRoomSchedule)
			return false;
		if (idSchedule != other.idSchedule)
			return false;
		if (movieName == null) {
			if (other.movieName != null)
				return false;
		} else if (!movieName.equals(other.movieName))
			return false;
		if (priceticket != other.priceticket)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ScheduleDto [idSchedule=" + idSchedule + ", idRoomSchedule=" + idRoomSchedule + ", movieName="
				+ movieName + ", startDate=" + startDate + ", dayOfWeek=" + dayOfWeek + ", priceticket=" + priceticket
				+ "]";
	}
	
}