package quanlirapphim.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeSchedule {
	private int idTime;
	private Date timeS;
	
	public TimeSchedule() {
		super();
	}
	
	public String getStrDate() {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		return format.format(timeS);
	}
	
	public String getStrTime() {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		return format.format(timeS);
	}
	public TimeSchedule(Date timeS) {
		super();
		this.timeS = timeS;
	}
	public int getIdTime() {
		return idTime;
	}
	public void setIdTime(int idTime) {
		this.idTime = idTime;
	}	
	public Date getTimeS() {
		return timeS;
	}
	public void setTimeS(Date timeS) {
		this.timeS = timeS;
	}
	@Override
	public String toString() {
		return "TimeSchedule [idTime=" + idTime + ", timeS=" + timeS + "]";
	}
	
}
