package quanlirapphim.model;

public class Room {
	private int idRoom ;
	private int numberofSeat ;
	private String propertyRoom;
	
	
	public Room(int idRoom, int numberofSeat, String propertyRoom) {
		super();
		this.idRoom = idRoom;
		this.numberofSeat = numberofSeat;
		this.propertyRoom = propertyRoom;
	}
	public Room() {
		super();
	}
	public int getIdRoom() {
		return idRoom;
	}
	public void setIdRoom(int idRoom) {
		this.idRoom = idRoom;
	}
	public int getNumberofSeat() {
		return numberofSeat;
	}
	public void setNumberofSeat(int numberofSeat) {
		this.numberofSeat = numberofSeat;
	}
	public String getPropertyRoom() {
		return propertyRoom;
	}
	public void setPropertyRoom(String propertyRoom) {
		this.propertyRoom = propertyRoom;
	}
	@Override
	public String toString() {
		return "Room [idRoom=" + idRoom + ", numberofSeat=" + numberofSeat + ", propertyRoom=" + propertyRoom + "]";
	}
	
	
}
