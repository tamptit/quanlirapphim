package quanlirapphim.model;


public class Schedule {
	private int idSchedule;
	private Room room;
	private Movie movie;
	private TimeSchedule startTime;
	private int priceTicket;
	
	
	public Schedule() {
		super();
	}
	
	public Schedule(int idSchedule, Room room, Movie movie, TimeSchedule startTime, int priceTicket) {
		super();
		this.idSchedule = idSchedule;
		this.room = room;
		this.movie = movie;
		this.startTime = startTime;
		this.priceTicket = priceTicket;
	}

	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	public TimeSchedule getStartTime() {
		return startTime;
	}
	public void setStartTime(TimeSchedule startTime) {
		this.startTime = startTime;
	}
	public int getIdSchedule() {
		return idSchedule;
	}
	public void setIdSchedule(int idSchedule) {
		this.idSchedule = idSchedule;
	}
	
	public int getPriceTicket() {
		return priceTicket;
	}
	public void setPriceTicket(int priceTicket) {
		this.priceTicket = priceTicket;
	}
//	public Date getStartDate() {
//		return startDate;
//	}
//	public void setStartDate(Date startDate) {
//		
//		this.startDate = startDate;
//	}
	@Override
	public String toString() {
		return "Schedule [idSchedule=" + idSchedule + ", room=" + room + ", movie=" + movie + ", startTime=" + startTime
				+ ", priceTicket=" + priceTicket + "]";
	}
	
	
	
	
}
