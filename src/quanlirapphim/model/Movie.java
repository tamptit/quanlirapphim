package quanlirapphim.model;


public class Movie{
	private int idMovie;
	private String nameMovie;
	private String typeMovie;
	private int timeMovie;
	private int year;
	
	public Movie() {
	}
	public Movie(int idMovie) {
		this.idMovie = idMovie;
	}
	public int getIdMovie() {
		return idMovie;
	}
	public void setIdMovie(int idMovie) {
		this.idMovie = idMovie;
	}
	public String getNameMovie() {
		return nameMovie;
	}
	public void setNameMovie(String nameMovie) {
		this.nameMovie = nameMovie;
	}
	public String getTypeMovie() {
		return typeMovie;
	}
	public void setTypeMovie(String typeMovie) {
		this.typeMovie = typeMovie;
	}
	public int getTimeMovie() {
		return timeMovie;
	}
	public void setTimeMovie(int timeMovie) {
		this.timeMovie = timeMovie;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	@Override
	public String toString() {
		return "Movie [idMovie=" + idMovie + ", nameMovie=" + nameMovie + ", typeMovie=" + typeMovie + ", timeMovie="
				+ timeMovie + "]";
	}
	
}
