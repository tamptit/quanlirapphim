package quanlirapphim.dao;

import java.util.List;

import quanlirapphim.model.Room;

public interface RoomDao {
	List<Room> getAllRoom();
	Room getRoom(int x);
	int getnumberofSeatRoom(int IdRoom);
	
	
}
