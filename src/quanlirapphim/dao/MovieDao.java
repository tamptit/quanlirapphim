package quanlirapphim.dao;

import java.sql.SQLException;
import java.util.List;

import quanlirapphim.model.Movie;

public interface MovieDao {
	List<Movie> getAllMovie() throws SQLException;
	Movie getMovie(int x) throws SQLException;
	String getNameMovie(Movie movie);
}
