package quanlirapphim.dao;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

import quanlirapphim.model.TimeSchedule;

public interface TimeScheduleDao {
	TimeSchedule getStartTime(int idTime);

	TimeSchedule geTimeSchedule(Date timeS);

	List<TimeSchedule> getAlltimeSchedule();

	boolean insertStartTime(TimeSchedule timeS) throws SQLIntegrityConstraintViolationException;
	
	boolean updateStarTime(TimeSchedule timS);
	List<Date> getTimebyDay(Date date1, int roomid);
}
