package quanlirapphim.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import quanlirapphim.dao.MovieDao;
import quanlirapphim.model.Movie;
import quanlirapphim.util.DBConnection;

public class MovieDaoImpl implements MovieDao{

	private Connection connect = null;
	@Override
	public List<Movie> getAllMovie() throws SQLException {
		connect = DBConnection.getConnect();
		List<Movie> listMovie = new ArrayList<>();
		try {
			Statement statement = connect.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM quanlirapphimm.`movie`;");
			while(resultSet.next()) {
				Movie movie = new Movie();
				movie.setIdMovie(resultSet.getInt("IdMovie"));
				movie.setNameMovie(resultSet.getString("nameMovie"));
				movie.setTypeMovie(resultSet.getString("TypeMovie"));
				movie.setYear(resultSet.getInt("YearRelease"));
				movie.setTimeMovie(resultSet.getInt("timeMovie"));
				listMovie.add(movie);
			}
			
			return listMovie;
			
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			connect.close();
		}
		
		return null;
	}

	@Override
	public Movie getMovie(int x) throws SQLException {
		Movie movie = new Movie();
		connect =  DBConnection.getConnect();
	    try {
			Statement statement = connect.createStatement();
			ResultSet resultSet = statement.executeQuery(""
					+ "SELECT * FROM quanlirapphimm.movie WHERE `IdMovie` = "+x+";");
			while (resultSet.next()) {
				movie.setIdMovie(resultSet.getInt("IdMovie"));;
				movie.setNameMovie(resultSet.getString("namemovie"));
				movie.setTypeMovie(resultSet.getString("TypeMovie"));
				movie.setYear(resultSet.getInt("YearRelease"));	
				movie.setTimeMovie(resultSet.getInt("timeMovie"));
			}
			
			return movie;
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			connect.close();
		}
		return null;
	}

	@Override
	public String getNameMovie(Movie movie) {
		// TODO Auto-generated method stub
		return null;
	}

}
