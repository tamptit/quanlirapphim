package quanlirapphim.dao.impl;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import quanlirapphim.dao.RoomDao;
import quanlirapphim.model.Room;
import quanlirapphim.util.DBConnection;

public class RoomDaoImpl implements RoomDao{
	private Connection connect = null;
	@Override
	public List<Room> getAllRoom() {
		// TODO Auto-generated method stub
		connect = DBConnection.getConnect();// tao mot object connection
		List<Room> listRoom = new ArrayList<Room>();//<>() notation gi day
		
		try {
			// Statement tao doi tuong creat...
			Statement statement= connect.createStatement();
			ResultSet rSet = statement.executeQuery("Select * From Room");
			//rset table of data. Lay du lieu ta dong goi (tao new Object() add vao list)
			while(rSet.next()) {
				Room room = new Room();
				room.setIdRoom(rSet.getInt(1));
				room.setNumberofSeat(rSet.getInt(2));
				room.setPropertyRoom(rSet.getString(3));
				listRoom.add(room); // truy van lay duoc cac thong tin tu table, 				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listRoom;
	}

	@Override
	public Room getRoom(int a) {
		connect = DBConnection.getConnect();
		Room room = new Room();
		try {
			Statement statement = connect.createStatement();
			ResultSet resultSet = statement.executeQuery(""
					+ "SELECT * FROM quanlirapphimm.room WHERE `IdRoom` = "+a+";");
			while(resultSet.next()) {
				room.setIdRoom(resultSet.getInt("IdRoom"));
				room.setNumberofSeat(resultSet.getInt("NumberofSeat"));
				room.setPropertyRoom(resultSet.getString("PropertyRoom"));
			}
			return room;
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int getnumberofSeatRoom(int IdRoom) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
