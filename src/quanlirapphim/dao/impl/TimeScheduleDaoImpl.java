package quanlirapphim.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import quanlirapphim.dao.TimeScheduleDao;
import quanlirapphim.model.TimeSchedule;
import quanlirapphim.util.DBConnection;

public class TimeScheduleDaoImpl implements TimeScheduleDao {

	private Connection connect;

	@Override
	public TimeSchedule getStartTime(int idTime) {
		TimeSchedule timeSchedule = new TimeSchedule();
		String sql = "SELECT * FROM quanlirapphimm.`time` WHERE `IdTime` = ?;";
		connect = DBConnection.getConnect();
		try {
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setInt(1, idTime);
			ResultSet resultSet = ps.executeQuery();		
			while (resultSet.next()) {
				timeSchedule.setIdTime(resultSet.getInt("IdTime"));
				timeSchedule.setTimeS(resultSet.getTimestamp("Starttime"));
			}
			// System.out.println("TimeSchedule: " + timeSchedule);
			connect.close();
			return timeSchedule;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<TimeSchedule> getAlltimeSchedule() {
		// TODO Auto-generated method stub
		List<TimeSchedule> listTimeSchedules = new ArrayList<TimeSchedule>();
		String sql = "SELECT * FROM quanlirapphimm.`time`";
		connect = DBConnection.getConnect();
		try {
			PreparedStatement ps = connect.prepareStatement(sql);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				TimeSchedule timeSchedule = new TimeSchedule();
				timeSchedule.setIdTime(resultSet.getInt("IdTime"));
				timeSchedule.setTimeS(resultSet.getTimestamp("Starttime"));
				listTimeSchedules.add(timeSchedule);
			}
			// System.out.println("TimeSchedule: " + timeSchedule);
			connect.close();
			return listTimeSchedules;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean insertStartTime(TimeSchedule timeS) throws SQLIntegrityConstraintViolationException {
		System.out.println("TimeSchedule: " + timeS);
		String sql = "INSERT INTO quanlirapphimm.`time` (Starttime) VALUES(?)";
		try (Connection connection = DBConnection.getConnect();
			PreparedStatement ps = connection.prepareStatement(sql);) {
			Date date = timeS.getTimeS();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String strDate = dateFormat.format(date.getTime());
			//System.out.println("timeS insert date: " + timeS);
			System.out.println("strDate insert: " + strDate);
			ps.setString(1, strDate);
			return ps.executeUpdate() > 0;
		} catch(SQLIntegrityConstraintViolationException e) {
			System.out.println("Duplicate time");
			throw e;
		}
		catch (SQLException e) {
			e.printStackTrace();
			System.out.println("insertStartTime(): exception occurs: " + e.getMessage());
		}
		return false;
	}

	@Override
	public TimeSchedule geTimeSchedule(Date timeS) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timeStr = formatter.format(timeS);
		System.out.println("timeStr : " + timeStr);
		String sql = "SELECT * FROM quanlirapphimm.`time` WHERE `Starttime` = ?";
		TimeSchedule timeSchedule = new TimeSchedule();
		try (Connection connection = DBConnection.getConnect();
			PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setString(1, timeStr);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				timeSchedule.setIdTime(resultSet.getInt("IdTime"));
				timeSchedule.setTimeS(resultSet.getDate("Starttime"));
			}
			System.out.println("compare: " + timeSchedule);
			return timeSchedule;
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return null;
	}

	@Override
	public List<Date> getTimebyDay(Date date1, int roomid) {
		// TODO Auto-generated method stub
		//List<TimeSchedule> listTimeSchedules = new ArrayList<TimeSchedule>();
		List<Date> listTime = new ArrayList<Date>();
		String sql = "SELECT s.`IdSchedule`, s.`RoomId`, m.`NameMovie`, t.`Starttime`, t.`DayOfweek`, s.`priceticket`\r\n" + 
				",`IdTime`\r\n" + 
				"FROM quanlirapphimm.`schedule` s \r\n" + 
				"INNER JOIN quanlirapphimm.`movie` m ON s.`MovieId` = m.`IdMovie`\r\n" + 
				"INNER JOIN quanlirapphimm.`time` t ON s.`TimeId` = t.`IdTime`\r\n" + 
				"WHERE DATE(`Starttime`) = ? order by `Starttime`";
		connect = DBConnection.getConnect();
		try (Connection connection = DBConnection.getConnect();
			PreparedStatement ps = connection.prepareStatement(sql);)
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String strDate = dateFormat.format(date1);
			ps.setString(1, strDate);
			//ps.setInt(2, roomid);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				Date date2 = resultSet.getTimestamp("Starttime");
				if (!listTime.contains(date2)) {
					listTime.add(date2);
				}
			}

			connect.close();
			System.out.println("listTime : " + listTime);
			return listTime;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean updateStarTime(TimeSchedule timeS) {
		// TODO Auto-generated method stub
		int id = timeS.getIdTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timeStr = formatter.format(timeS.getTimeS());
		System.out.println("timeStr : " + timeStr);
		String sql = "UPDATE quanlirapphimm.time\r\n" + 
				"SET Starttime= ?\r\n" + 
				"WHERE `IdTime` = ?";
		//TimeSchedule timeSchedule = new TimeSchedule();
		try (Connection connection = DBConnection.getConnect();
			PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setString(1, timeStr);
			ps.setInt(2, id);
			TimeSchedule timeSchedule2 = new TimeSchedule();
			timeSchedule2.setIdTime(id); 
			timeSchedule2.setTimeS(timeS.getTimeS());
			System.out.println("update2: " + timeSchedule2);
			if(ps.executeUpdate()> 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return false;
	}

}
