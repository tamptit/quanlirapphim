package quanlirapphim.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import quanlirapphim.dao.MovieDao;
import quanlirapphim.dao.RoomDao;
import quanlirapphim.dao.ScheduleDao;
import quanlirapphim.dao.TimeScheduleDao;
import quanlirapphim.dto.ScheduleDto;
import quanlirapphim.model.Schedule;
import quanlirapphim.util.DBConnection;

public class ScheduleDaoImpl implements ScheduleDao {
	private Connection connect = null;

	private String parseDate(int dayOfWeek) {
		String res = "";
		switch (dayOfWeek) {
		case 2:
			res = "Monday";
			break;
		case 3:
			res = "Tuesday";
			break;
		case 4:
			res = "Wednesday";
			break;
		case 5:
			res = "Thursday";
			break;
		case 6:
			res = "Friday";
			break;
		case 7:
			res = "Saturday";
			break;
		case 8:
			res = "Sunday";
			break;
		default:
			res = "oke";
			break;
		}
		return res;
	}

	@Override
	public List<Schedule> getAllSchedule() throws SQLException {
		connect = DBConnection.getConnect();
		List<Schedule> listSchedule = new ArrayList<Schedule>();
		try {
			Statement statement = connect.createStatement();
			ResultSet rSet = statement.executeQuery(
					"SELECT s.`IdSchedule`, s.`RoomId`,s.`MovieId`, m.`NameMovie`, t.`Starttime`, t.`DayOfweek`, s.`priceticket` , r.`NumberofSeat` , r.`PropertyRoom`\r\n"
							+ "FROM quanlirapphimm.`schedule` s \r\n"
							+ "INNER JOIN quanlirapphimm.`movie` m ON s.`MovieId` = m.`IdMovie`\r\n"
							+ "INNER JOIN quanlirapphimm.`time` t ON s.`TimeId` = t.`IdTime`\r\n"
							+ "INNER JOIN quanlirapphimm. `room` r ON s.`RoomId` = r.`IdRoom`;");
			while (rSet.next()) {
				Schedule schedule = new Schedule();
				RoomDao room = new RoomDaoImpl();
				MovieDao movie = new MovieDaoImpl();
				TimeScheduleDao time = new TimeScheduleDaoImpl();
				schedule.setIdSchedule(rSet.getInt(1));
				schedule.setRoom(room.getRoom(rSet.getInt("RoomId")));
				schedule.setMovie(movie.getMovie(rSet.getInt("MovieId")));
				schedule.setStartTime(time.geTimeSchedule(rSet.getTimestamp("Starttime")));
				schedule.setPriceTicket(rSet.getInt("priceticket"));
				listSchedule.add(schedule);
			}
			// System.out.println("listscd" + listSchedule.toString());
			return listSchedule;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		return null;
	}

	@Override
	public List<ScheduleDto> getAllScheduleHomeScreen() throws SQLException {
		connect = DBConnection.getConnect();
		List<ScheduleDto> listSchedule = new ArrayList<ScheduleDto>();
		try {
			Statement statement = connect.createStatement();
			ResultSet rSet = statement.executeQuery(
					"SELECT s.`IdSchedule`, s.`RoomId`, m.`NameMovie`, t.`Starttime`, t.`DayOfweek`, s.`priceticket`\r\n"
							+ "FROM quanlirapphimm.`schedule` s \r\n"
							+ "INNER JOIN quanlirapphimm.`movie` m ON s.`MovieId` = m.`IdMovie`\r\n"
							+ "INNER JOIN quanlirapphimm.`time` t ON s.`TimeId` = t.`IdTime`\r\n"
							+ "-- WHERE t.`Starttime` >= NOW()\r\n" + "ORDER BY s.`IdSchedule` DESC;");
			while (rSet.next()) {
				ScheduleDto schedule = new ScheduleDto();
				schedule.setIdSchedule(rSet.getInt("IdSchedule"));
				schedule.setIdRoomSchedule(rSet.getInt("RoomId"));
				schedule.setMovieName(rSet.getString("NameMovie"));
				// schedule.setStartDate(rSet.getDate("Starttime"));
				schedule.setStartDate(rSet.getTimestamp("Starttime"));
				schedule.setDayOfWeek(parseDate(rSet.getInt("DayOfweek")));
				schedule.setPriceticket(rSet.getInt("priceticket"));
				listSchedule.add(schedule);
			}
			
			return listSchedule;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connect.close();
		}
		return null;
	}

	@Override
	public List<Schedule> searchScheduleRoom(int idSearch) throws SQLException {
		// TODO Auto-generated method stub
		List<Schedule> listSchedule = new ArrayList<Schedule>();
		//String teString = "search Room ScheduleDAOImpl";
		connect = DBConnection.getConnect();
		try {
			Statement statement = connect.createStatement();
			ResultSet resultSet = statement.executeQuery(""
					+ "SELECT s.`IdSchedule`, s.`RoomId`,  s.`MovieId`, t.`IdTime`, t.`DayOfweek`, s.`priceticket` \r\n"
					+ "FROM quanlirapphimm.`schedule` s\r\n"
					+ "INNER JOIN quanlirapphimm.`movie` m ON s.`MovieId` = m.`IdMovie`\r\n"
					+ "INNER JOIN quanlirapphimm.`time` t ON s.`TimeId` = t.`IdTime`\r\n" + "WHERE `RoomID` = "
					+ idSearch + ";");

			RoomDao roomDao = new RoomDaoImpl();
			MovieDao movieDao = new MovieDaoImpl();
			TimeScheduleDao timeDao = new TimeScheduleDaoImpl();
			while (resultSet.next()) {
				Schedule schedule = new Schedule();
				schedule.setIdSchedule(resultSet.getInt(1));
				schedule.setRoom(roomDao.getRoom(resultSet.getInt("RoomId")));
				schedule.setMovie(movieDao.getMovie(resultSet.getInt("MovieId")));	
				schedule.setStartTime(timeDao.getStartTime(resultSet.getInt("IdTime")));
				schedule.setPriceTicket(resultSet.getInt("priceticket"));
				listSchedule.add(schedule);
			}
			
			System.out.println("listTest: " + listSchedule);
			return listSchedule;
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			connect.close();
		}

		return null;
	}

	@Override
	public List<Schedule> searchScheduleMovie(String a) throws SQLException {
		connect = DBConnection.getConnect();
		List<Schedule> listSchedule = new ArrayList<Schedule>();
		try {
			Statement statement = connect.createStatement();
			ResultSet resultSet = statement.executeQuery(
					"SELECT s.`IdSchedule`, s.`RoomId`, s.`MovieId`,m.`NameMovie`, t.`IdTime`, t.`DayOfweek`, s.`priceticket` \r\n"
							+ "FROM quanlirapphimm.`schedule` s\r\n"
							+ "INNER JOIN quanlirapphimm.`movie` m ON s.`MovieId` = m.`IdMovie`\r\n"
							+ "INNER JOIN quanlirapphimm.`time` t ON s.`TimeId` = t.`IdTime`\r\n"
							+ "WHERE `NameMovie` like '%" + a + "%';");

			RoomDao roomDao = new RoomDaoImpl();
			MovieDao movieDao = new MovieDaoImpl();
			TimeScheduleDao timeDao = new TimeScheduleDaoImpl();
			while (resultSet.next()) {
				Schedule schedule = new Schedule();
				schedule.setIdSchedule(resultSet.getInt(1));
				schedule.setRoom(roomDao.getRoom(resultSet.getInt("RoomId")));
				schedule.setMovie(movieDao.getMovie(resultSet.getInt("MovieId")));
				// schedule.setStartDate(rSet.getTimestamp("Starttime"));
				schedule.setStartTime(timeDao.getStartTime(resultSet.getInt("IdTime")));
				schedule.setPriceTicket(resultSet.getInt("priceticket"));
				listSchedule.add(schedule);
			}
			
			
			System.out.println("listTest: " + listSchedule);
			return listSchedule;
			// System.out.println("listtoRoom: " + listSchedule);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			connect.close();
		}
		return null;
	}

	@Override
	public ScheduleDto getScheduleDto(int i) throws SQLException {
		ScheduleDto dto = new ScheduleDto();
		connect = DBConnection.getConnect();
		try {
			Statement statement = connect.createStatement();
			ResultSet set = statement.executeQuery(
					"SELECT s.`IdSchedule`, s.`RoomId`, m.`NameMovie`, t.`Starttime`, t.`DayOfweek`, s.`priceticket` \r\n"
							+ "FROM quanlirapphimm.`schedule` s\r\n"
							+ "INNER JOIN quanlirapphimm.`movie` m ON s.`MovieId` = m.`IdMovie`\r\n"
							+ "INNER JOIN quanlirapphimm.`time` t ON s.`TimeId` = t.`IdTime`\r\n"
							+ "WHERE `IdSchedule` = 1;");
			while (set.next()) {
				dto.setIdSchedule(set.getInt("IdSchedule"));
				dto.setIdRoomSchedule(set.getInt("RoomId"));
				dto.setMovieName(set.getString("NameMovie"));
				dto.setStartDate(set.getTimestamp("Starttime"));
				dto.setDayOfWeek(parseDate(set.getInt("DayOfweek")));
				dto.setPriceticket(set.getInt("priceticket"));
			}
			return dto;
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			connect.close();
		}
		return null;
	}

	@Override
	public int getIdSchedule() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Schedule getSchedule(int id) throws SQLException {
		connect = DBConnection.getConnect();

		MovieDao movie = new MovieDaoImpl();
		RoomDao room = new RoomDaoImpl();
		TimeScheduleDao time = new TimeScheduleDaoImpl();
		try {
			Statement statement = connect.createStatement();
			ResultSet resultSet = statement
					.executeQuery("" + "SELECT * FROM quanlirapphimm.`schedule` WHERE `IdSchedule` = " + id + ";");
			Schedule schedule = null;
			while (resultSet.next()) {
				schedule = new Schedule();
				schedule.setIdSchedule(resultSet.getInt("IdSchedule"));
				schedule.setMovie(movie.getMovie(resultSet.getInt("MovieId")));
				schedule.setRoom(room.getRoom(resultSet.getInt("RoomId")));
				schedule.setStartTime(time.getStartTime(resultSet.getInt("TimeId")));
				schedule.setPriceTicket(resultSet.getInt("priceticket"));

			}	
			return schedule;
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			connect.close();
		}
		return null;
	}

	@Override
	public void saveSchedule(Schedule schedule) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean updateSchedule(Schedule schedule) throws SQLException {
//		TimeScheduleDao timeDao = new TimeScheduleDaoImpl();
//		if (schedule.getStartTime().getTimeS() != null) {
//			System.out.println("update time: " +schedule.getStartTime().getTimeS());
//			timeDao.insertStartTime(schedule.getStartTime().getTimeS());
//		}
		Connection conn = DBConnection.getConnect();
		System.out.println("schedule update: " + schedule);
		String sql = "UPDATE Schedule SET RoomId = ?, MovieId = ?, TimeId = ?,"
				+ "priceticket = ? WHERE IdSchedule = ?";
		try ( 
				PreparedStatement ps = conn.prepareStatement(sql);) {
			ps.setInt(1, schedule.getRoom().getIdRoom());
			ps.setInt(2, schedule.getMovie().getIdMovie());
			ps.setInt(3, schedule.getStartTime().getIdTime());
			ps.setInt(4, schedule.getPriceTicket());
			ps.setInt(5, schedule.getIdSchedule());
			return ps.executeUpdate() > 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
		return false;
	}

	@Override
	public List<Schedule> getAllSchedulebyDate(Date timeS) throws SQLException {
		List<Schedule> listSchedulebyDate = new ArrayList<Schedule>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String timeStr = formatter.format(timeS);
		System.out.println("timeStr : " + timeStr);
		String sql = "SELECT s.`IdSchedule`, s.`RoomId`, m.`IdMovie`,m.`NameMovie`, t.`Starttime`, t.`DayOfweek`, s.`priceticket` ,`IdTime`,\r\n" + 
				"DATE(t.`Starttime`) as `Date`, Time(t.`Starttime`) as `Time` , m.`timeMovie`\r\n" + 
				"FROM quanlirapphimm.`schedule` s  \r\n" + 
				"INNER JOIN quanlirapphimm.`movie` m ON s.`MovieId` = m.`IdMovie`\r\n" + 
				"INNER JOIN quanlirapphimm.`time` t ON s.`TimeId` = t.`IdTime`\r\n" + 
				"WHERE DATE(`Starttime`) = ? order by `Starttime`";
		Connection connection = DBConnection.getConnect();
		try (
			PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setDate(1, timeS);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				MovieDao movie = new MovieDaoImpl();
				RoomDao room = new RoomDaoImpl();
				TimeScheduleDao time = new TimeScheduleDaoImpl();
				Schedule schedule = new Schedule();
				schedule.setIdSchedule(resultSet.getInt("IdSchedule"));
				schedule.setMovie(movie.getMovie(resultSet.getInt("IdMovie")));
				schedule.setRoom(room.getRoom(resultSet.getInt("RoomId")));
				schedule.setStartTime(time.geTimeSchedule(resultSet.getTimestamp("Starttime")));
				schedule.setPriceTicket(resultSet.getInt("priceticket"));
				listSchedulebyDate.add(schedule);
			}
			
			System.out.println("byDate: " + listSchedulebyDate);
			
			return listSchedulebyDate;
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			connection.close();
		}
		return null;
	}

}
