package quanlirapphim.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import quanlirapphim.dto.ScheduleDto;
import quanlirapphim.model.Schedule;

public interface ScheduleDao {
	List<Schedule> getAllSchedule() throws SQLException;
	List<Schedule> getAllSchedulebyDate(Date timeS) throws SQLException;
	List<ScheduleDto> getAllScheduleHomeScreen()  throws SQLException;
	List<Schedule> searchScheduleRoom(int a) throws SQLException;
	List<Schedule> searchScheduleMovie(String a) throws SQLException;
	int getIdSchedule();
	//List<Schedule> searchRoom(int a);
	//List<Schedule> searchMovie(String a);
	ScheduleDto getScheduleDto(int id) throws SQLException;
	Schedule getSchedule(int id) throws SQLException;
	
	void saveSchedule(Schedule schedule);
	boolean updateSchedule(Schedule schedule) throws SQLException;

}
