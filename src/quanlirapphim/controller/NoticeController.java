package quanlirapphim.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quanlirapphim.model.Schedule;

//import quanlirapphim.model.Schedule;

/**
 * Servlet implementation class NoticeController
 */
@WebServlet("/notice")
public class NoticeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public NoticeController() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		Schedule schedule =  (Schedule) request.getAttribute("schedules");
		Boolean update = (Boolean) request.getAttribute("update");
		request.setAttribute("myname", update);
//		RequestDispatcher rd = request.getRequestDispatcher("/search-schedule");  // hoi lai Dung
		RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/jsps/listScheduleMovie.jsp");
//		rd.forward(request, response);
		out.println("<html>");
		out.println("<head><title>Notice Update</title></head>");
		out.println("<body>");
		out.println("<h3>Hello Cinema</h3>");
		out.println("Update:" + update);
		out.println("<p>Movie:" + schedule.getMovie() +"</p>");
		out.println("<p>Room:" + schedule.getRoom() +"</p>");
		out.println("<p>Time:" + schedule.getStartTime() +"</p>");
		out.println("</body>");
		out.println("</html>");
	       
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("UTF-8");
		this.doGet(request, response);
	}

}
