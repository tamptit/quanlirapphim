package quanlirapphim.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quanlirapphim.dao.MovieDao;
import quanlirapphim.dao.RoomDao;
import quanlirapphim.dao.ScheduleDao;
import quanlirapphim.dao.TimeScheduleDao;
import quanlirapphim.dao.impl.MovieDaoImpl;
import quanlirapphim.dao.impl.RoomDaoImpl;
import quanlirapphim.dao.impl.ScheduleDaoImpl;
import quanlirapphim.dao.impl.TimeScheduleDaoImpl;
import quanlirapphim.model.Movie;
import quanlirapphim.model.Room;
import quanlirapphim.model.Schedule;
import quanlirapphim.model.TimeSchedule;

@WebServlet("/schedule/*")
public class ScheduleController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MovieDao movieDao = null;
	private RoomDao roomDao = null;
	private TimeScheduleDao timeDao = null;
	private ScheduleDao scheduleDao = null;

	public ScheduleController() {
		super();
		movieDao = new MovieDaoImpl();
		roomDao = new RoomDaoImpl();
		timeDao = new TimeScheduleDaoImpl();
		scheduleDao = new ScheduleDaoImpl();
	}

	protected void selectAvailable(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException{
		String idString = request.getParameter("id");
		System.out.println("idparam: " + request.getParameter("idparam"));
//		if (idString != null) {
//			int id = Integer.parseInt(idString);  // bug Interger.parseInt()
//			Schedule schedule = scheduleDao.getSchedule(id);
//			if (schedule == null) {
//				System.out.println("SerInfo: " + getServletInfo() +"\nSerName: " + getServletName() +"\nContext: " + getServletContext() );
//				response.sendRedirect("./search-schedule");
//				return;
//			}
//		}
		// get Schedule
		int id = Integer.parseInt(idString);
		Schedule schedule = scheduleDao.getSchedule(id);
		List<Room> rooms = roomDao.getAllRoom(); // getAll cho combox
		List<Movie> movies;
		try {
			movies = movieDao.getAllMovie();
			request.setAttribute("movies", movies); // box
		} catch (SQLException e) {
			
			e.printStackTrace();
		} // getAll cho combox
		TimeSchedule timeSchedules = new TimeSchedule();
		
		request.setAttribute("rooms", rooms); // box
		request.setAttribute("times", timeSchedules);
		request.setAttribute("schedule", schedule);
		request.setAttribute("editId", idString);
		request.setAttribute("idSchedule", schedule.getIdSchedule());
		request.setAttribute("idRoom", schedule.getRoom().getIdRoom()); // get select box Room
		request.setAttribute("nameMovie", schedule.getMovie().getNameMovie());
		request.setAttribute("startTime", schedule.getStartTime().getTimeS()); // get datetime
		request.setAttribute("showDate", schedule.getStartTime().getStrDate()); // get date
		request.setAttribute("showTime", schedule.getStartTime().getStrTime());// get time
		request.setAttribute("priceticket", schedule.getPriceTicket());
		System.out.println("ID : " + idString + "\nschedule: " + schedule);
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("idparam: " + request.getParameter("idparam"));
		try {
			selectAvailable(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsps/edit-schedule.jsp");
		dispatcher.forward(request, response);
		
//		String idString = request.getParameter("id");
//		if (idString != null) {
//		int id = Integer.parseInt(idString);
//		Schedule schedule = scheduleDao.getSchedule(id);
//		if (schedule != null)
//		List<Room> rooms = roomDao.getAllRoom(); // getAll cho combox
//		List<Movie> movies = movieDao.getAllMovie(); // getAll cho combox
//		TimeSchedule timeSchedules = new TimeSchedule();
//		System.out.println("ID : " + idString + "\nschedule: " + schedule);
//		listSchedulebyDate(request,response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// doGet(request, response);
		int strId = Integer.parseInt(request.getParameter("idparam"));
		System.out.println("param id:" + strId);
		Schedule schedule = null;
		TimeSchedule timeSchedule = null;
		Date timeUpdate = null;
		String timeStrUpdate = null;
		int timeId = 0;
		timeStrUpdate = request.getParameter("showTime") + " " + request.getParameter("showDate");	
		DateFormat dateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy");
		try {
			schedule = scheduleDao.getSchedule(strId);
			timeId = schedule.getStartTime().getIdTime();
			timeSchedule = timeDao.getStartTime(timeId);
			timeDao.updateStarTime(timeSchedule); // update Time vao
			timeUpdate = dateFormat.parse(timeStrUpdate);
			timeSchedule.setTimeS(timeUpdate);// set lai thoi gian
		} catch (SQLException | ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			System.out.println("idTIme: " + timeId);
			System.out.println("Test timeUpdate: " + timeStrUpdate); // lay date time from 
			System.out.println("timeSchedule Update: " + timeSchedule);
		}

		// code get lai information khi updateTime loi
		//selectAvailable(request, response);
		// doGet(request, response);
		
		int scheduleId =  Integer.parseInt(request.getParameter("idSchedule")); // ep kieu int co the gay bug.
		int roomId =  Integer.parseInt(request.getParameter("roomId"));
		int moiveId = Integer.parseInt(request.getParameter("movieId"));
		int price =  Integer.parseInt(request.getParameter("price")); 
		System.out.println("scheduleId " + scheduleId);
		
		// -- set Schedule update vao table
		schedule.setIdSchedule(scheduleId); // lay idSchedule
		schedule.setRoom(roomDao.getRoom(roomId));// lay room
		try {
			schedule.setMovie(movieDao.getMovie(moiveId));
		} catch (SQLException e) {
			e.printStackTrace();
		} // lay movie
		schedule.setStartTime(timeSchedule);
		// schedule.setStartTime(timeDao.geTimeSchedule(date));// lay time
		schedule.setPriceTicket(price);
		request.setAttribute("updateSchedule", schedule);
		request.setAttribute("tblUpdate", "true");
		boolean b = false;
		try {
			b = scheduleDao.updateSchedule(schedule);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (b) {
			request.setAttribute("schedules", schedule);
			request.setAttribute("update", true);
			String context = request.getContextPath();
			RequestDispatcher rd = request.getRequestDispatcher("/notice");
			
			System.out.println("ContexPath: " + request.getContextPath());
			System.out.println("update!! Check DB" );
			rd.forward(request, response);
			return;
		} else {
			request.setAttribute("update", false);
			RequestDispatcher rd= request.getServletContext().getRequestDispatcher("/notice");  
			rd.forward(request, response);//method may be include or forward  
			System.out.println("update false");
		}
		//response.sendRedirect("./schedule?id=" + strId); // chuyen sang trang
	}
	
	protected Schedule getScheduleEdit(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {  
		String idString = request.getParameter("id");
		System.out.println("idparam: " + request.getParameter("idparam"));
		int id = Integer.parseInt(idString); // khong can kiem tra null nua
		Schedule schedule = null;
		try {
			schedule = scheduleDao.getSchedule(id);
			System.out.println("schedule: " + schedule);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return schedule;
		
	}   // dung chung de getSchedule 
	/*
	protected void listSchedulebyDate(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Schedule schedule = getScheduleEdit(request, response);
		Date timS = schedule.getStartTime().getTimeS();
		System.out.println("byDate: " + timS);
		request.setAttribute("listSchedulebyDate", timS);
		// DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:");
	}
	
	protected void checkTimebyDate(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// get Schedule
		Schedule schedule = getScheduleEdit(request, response);
		int timeId = schedule.getStartTime().getIdTime(); // get id Time
		System.out.println("idTIme: " + timeId);
		String timeStrUpdate = request.getAttribute("showTime") + " " + request.getAttribute("showDate");// lay time update form jsp
		
		DateFormat dateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy");
		Date timeUpdate;
		TimeSchedule timeSchedule = timeDao.getStartTime(timeId);  
		try {
			timeUpdate = dateFormat.parse(timeStrUpdate);
			timeSchedule.setTimeS(timeUpdate);// set lai thoi gian
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/

}
