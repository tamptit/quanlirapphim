package quanlirapphim.controller;

import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.ZoneId;
//import java.time.ZonedDateTime;
import java.util.TimeZone;

public class Timezone {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
        Date currentDate = calendar.getTime();
        calendar.add(Calendar.MINUTE, 90);
        String plusDate = format.format(calendar.getTime());
        System.out.println("current: " + currentDate);
        System.out.println("plusDate: " + plusDate);
        
	}

}
