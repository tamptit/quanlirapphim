package quanlirapphim.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quanlirapphim.dao.RoomDao;
import quanlirapphim.dao.impl.RoomDaoImpl;
import quanlirapphim.model.Room;

/**
 * Servlet implementation class AddRoomControler
 */
@WebServlet("/listPhong")
public class ListRoomControler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private RoomDao roomDAO = null; // de lam gi

    public ListRoomControler() {// contructor
        super();
        roomDAO = new RoomDaoImpl();// ?
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		List<Room> abc = roomDAO.getAllRoom();
		int Size = abc.size();
		Boolean ktra = abc.isEmpty();
		request.setAttribute("listRoom", abc);
		request.setAttribute("myname", "TammAnhh");
		request.setAttribute("num", Size);
		request.setAttribute("check", ktra);
		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/jsps/listScheduleMovie.jsp");	
		//RequestDispatcher dispatcher = request.getRequestDispatcher(des);	
		dispatcher.forward(request, response);
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
