package quanlirapphim.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quanlirapphim.dao.ScheduleDao;
import quanlirapphim.dao.impl.ScheduleDaoImpl;
import quanlirapphim.model.Schedule;

@WebServlet("/search-schedule")
public class SearchSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private ScheduleDao scheduleDAO = null;  
    
    public SearchSchedule() {
        super();
        scheduleDAO = new ScheduleDaoImpl();  
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("UTF-8");
//		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
//        ServletOutputStream out = response.getOutputStream();
//        out.println("<html>");
//        out.println("<head><title>Init Param</title></head>");
//        out.println("<body>");
//        out.println("<h3>Init Param</h3>");
//        out.println("</body>");
//        out.println("<html>");
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/jsps/listScheduleMovie.jsp");
		dispatcher.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
        
		String getInput = request.getParameter("searchInput");
		request.setAttribute("inputroom", null);
		if (request.getParameter("choiceaction") != null) {
			System.out.println("oke action");
		}
		if (getInput != null) {
			if ( request.getParameter("searchRoom") != null  )  {
				if (!getInput.matches("(?<=\\s|^)\\d+(?=\\s|$)")) {
					request.setAttribute("inputroom", "Please input digit only to search room");
				}else {
					searchtoRoom(request, response);
				}
			}
			if (request.getParameter("searchMovie") != null) {
				searchtoMovie(request, response);
			}
		}
		
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/jsps/listScheduleMovie.jsp");
		dispatcher.forward(request, response);
	}
	
	protected void searchtoRoom(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException {
		
//		request.setCharacterEncoding("UTF-8");
//		response.setCharacterEncoding("UTF-8");
		
    	String getInputRoom = request.getParameter("searchInput"); 
		int getInputSearch = Integer.parseInt(getInputRoom);
		request.setAttribute("myname", getInputSearch);
		List<Schedule> listSchedule = null;
		try {
			listSchedule = scheduleDAO.searchScheduleRoom( getInputSearch);	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			request.setAttribute("check", listSchedule.isEmpty());
			System.out.println("listCtr: " + listSchedule);
			request.setAttribute("schedules", listSchedule);
		}
		
	}
	
	protected void searchtoMovie(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException {
		
//		request.setCharacterEncoding("UTF-8");
//		response.setCharacterEncoding("UTF-8");
        
		//if (request.getParameter("searchMovie") != null) {
		String getInputSearch = request.getParameter("searchInput");
		request.setAttribute("myname", getInputSearch);
		List<Schedule> listSchedule = null;
		try {
			listSchedule = scheduleDAO.searchScheduleMovie(getInputSearch);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			request.setAttribute("check", listSchedule.isEmpty());	
			request.setAttribute("schedules", listSchedule);
			System.out.println("list: " + listSchedule);
		}
	}


}
