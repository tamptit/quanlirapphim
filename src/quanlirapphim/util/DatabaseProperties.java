package quanlirapphim.util;


import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class DatabaseProperties {
	static Map<String,String> map= new HashMap<String, String>();
	static Properties prop = new Properties();
	static {
		InputStream input = null;
		try {
			input = DatabaseProperties.class.getClassLoader().getResourceAsStream("config.database.properties");
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		Set<Entry<Object, Object>> set =  prop.entrySet();  
		for (Entry<Object, Object> o : set) {
			Entry<Object, Object> entry =  o;
			map.put(entry.getKey().toString(), entry.getValue().toString());
			//System.out.printf("%s = %s%n", entry.getKey(), entry.getValue());
		}
	}
	
	public static String getData(String key) {
		String string = "";
		if (map.containsKey(key)) {
			string = map.get(key);
		}
		//System.out.println("kq: " + string);
		return string;
	}
/*	
	public static void main(String[] args) {
		System.out.println("Results Here:");
		Set<Entry<Object, Object>> set =  prop.entrySet();  
		for (Entry<Object, Object> o : set) {
			Entry<Object, Object> entry =  o;
			map.put(entry.getKey().toString(), entry.getValue().toString());
			//System.out.printf("+ %s = %s%n", entry.getKey(), entry.getValue());
		}
		DatabaseProperties db = new DatabaseProperties();
		db.getData("password");
	}
*/
}

	

