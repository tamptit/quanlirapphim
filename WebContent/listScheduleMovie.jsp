<%@page import="quanlirapphim.model.Schedule"%>
<%@page import="quanlirapphim.model.Room"%>
<%@page import="quanlirapphim.dao.impl.ScheduleDaoImpl"%>
<%@page import="quanlirapphim.dto.ScheduleDto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<title>Lịch chiếu</title>
</head>
<body>
	<p>My Name:"${myname}"</p>

	<!-- Search form -->
	<div class="container" style="padding: 20px;">

		<div class="flex-div">
			<h3>Tìm kiếm lịch chiếu</h3>
			<form action="search-schedule" method="post" name="frmsearch">
				<div class="row">
					<div class="col-sm-6">
						<input class="form-control" type="text" name="searchInput"
							placeholder="Search" aria-label="Search" required> <span
							class="error"> <c:if test="${inputroom != null}">"${inputroom}"</c:if>
						</span>
					</div>
					<div class="col-sm-6">
						<input type=submit value="Tìm Phòng" name="searchRoom"> <input
							type=submit value="Tìm Phim" name="searchMovie">
					</div>
				</div>
			</form>
		</div>
		<div class="flex-div" style="padding-top: 30px;">
			<h3>Danh sách lịch chiếu</h3>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>STT</th>
						<th>Phòng</th>
						<th>Phim</th>
						<th>Giờ chiếu</th>
						<th>Giá</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:set var="num" value="1" />
					<c:forEach items="${schedules}" var="ls" varStatus="i">
						<tr>
							<td>${num}</td>
							<td>${ls.room.idRoom}</td>
							<td>${ls.movie.nameMovie}</td>
							<td>
								<p>
									<fmt:formatDate type="both" dateStyle="short" timeStyle="short"
										value="${ls.startTime.timeS}" />
								</p>
							</td>
							<td>${ls.priceTicket}</td>
							<!--  -->
							<td><a href="schedule?id=${ls.idSchedule}">
									<button type="button" class="btn btn-light" name="choiceaction">Edit</button>
							</a> ${ls.idSchedule}</td>
						</tr>

						<c:set var="num" value="${num + 1}" />
					</c:forEach>
				</tbody>
			</table>
			<!-- end table -->
		</div>
	</div>
</body>

</html>