<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách Phòng</title>
</head>
<body>
	<div class="container" style="padding: 20px;">
		<p>My Name:"${myname}"</p>
		<%= request.getAttribute("check")%>
		<%= request.getAttribute("myname") %>
		<%= request.getAttribute("num") %>

		<table class="table table-bordered" id="myTable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Số ghế</th>
					<th>Đặc điểm</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="room" items="${listRoom}">
					<tr>
						<td>${room.getIdRoom() }</td>
						<td>${room.getNumberofSeat()}</td>
						<td>${room.getPropertyRoom()}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div class="flex-div" style="padding-top: 30px;">
			<h3>List Room</h3>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Số ghế</th>
						<th>Đặc điểm</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>John</td>
						<td>Doe</td>
						<td>john@example.com</td>
						<td>
							<button type="button" class="btn btn-link">Edit</button>
						</td>
					</tr>
					<tr>
						<td>Mary</td>
						<td>Moe</td>
						<td>mary@example.com</td>
						<td>
							<button type="button" class="btn btn-link">Edit</button>
						</td>
					</tr>
					<tr>
						<td>July</td>
						<td>Dooley</td>
						<td>july@example.com</td>
						<td>
							<button type="button" class="btn btn-link">Edit</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>


