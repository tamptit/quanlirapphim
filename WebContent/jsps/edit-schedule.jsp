<%@page import="quanlirapphim.model.Schedule"%>
<%@page import="quanlirapphim.dao.impl.ScheduleDaoImpl"%>
<%@page import="quanlirapphim.dao.ScheduleDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css"
	rel="stylesheet" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


<title>Sửa lịch chiếu</title>
</head>
<body>
	<p>My Edit: "${editId}"</p>
	<!-- Search form -->
	<div class="container" style="margin: 20px;">
		<c:if test="${param.message eq 'success'}">
			<div class="alert alert-success">
				<strong>Success!</strong> Indicates a successful or positive action.
			</div>
		</c:if>
		<div style="width: 50%; float: left">
			<form action="schedule" method="post">
				<div class="form-group row" style="display: none">
					<input id="id" name="idSchedule" value="${schedule.idSchedule}">
				</div>
				<div class="form-group row">
					<label for="inputRoomId" class="col-sm-3 col-form-label">Phòng:
					</label>
					<div class="col-sm-6" style="float: left">
						<select id="room" name="roomId" class="form-control">
							<c:forEach items="${rooms}" var="room">
								<option value="${room.idRoom}"
									${room.idRoom == schedule.room.idRoom ? "selected" : ""}>${room.idRoom}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="inputMoiveId" class="col-sm-3 col-form-label">Phim:
					</label>
					<div class="col-sm-6" style="float: left">
						<select id="nameMovie" name="movieId" class="form-control">
							<c:forEach items="${movies}" var="movie">
								<option value="${movie.idMovie}"
									${movie.idMovie == schedule.movie.idMovie ? "selected" : ""}>${movie.nameMovie}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				${startTime}
				<div class="form-group row">
					<label for="inputStartTime" class="col-sm-3 col-form-label">Ngày
						chiếu: </label>
					<div class="col-sm-6" style="float: left">
						<fmt:formatDate type="date" pattern="dd-MM-yyyy" 
						value="${startTime}" var="fomatDatePicker" />
						<input id="pickerDate" name="showDate" type="text" 
						class="form-control" value="${fomatDatePicker}" readonly>
					</div>
				</div>

				<div class="form-group row">
					<label for="inputStartTime" class="col-sm-3 col-form-label">Giờ
						chiếu: </label>
					<div class="col-sm-6" style="float: left">
						<fmt:formatDate type="time" pattern="HH:mm" value="${startTime}"
							var="fomatTimePicker" />
						<input id="pickerTime" name="showTime" type="text"
							class="form-control" value="${fomatTimePicker}" readonly>
					</div>
				</div>

				<div class="form-group row">
					<label for="inputPrice" class="col-sm-3 col-form-label">Giá
						vé: </label>
					<div class="col-sm-6" style="float: left">
						<input id="price" name="price" type="number" min="1000"
							max="1000000000" class="form-control" value="${priceticket}">
					</div>
				</div>

				<input type="hidden" name="idparam" value="${param.id}" /> <input
					type="hidden" name="setTime"
					value="${fomatTimePicker} ${fomatDatePicker}" />
				<div class="form-group row">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>

			</form>
			
		</div>
		<div style="float: left; width: 50%">
			<div style="display: block">
				<p>listByDate: ${listSchedulebyDate}</p>
			</div>
		</div>

		<c:set var="listSchedulebyDate" scope="session" value="${null}" />
		<c:out value="${salary}" />

	</div>	
	
	<script>
		src = "https://code.jquery.com/jquery-3.4.1.min.js"
	</script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
	<script>
		$('#pickerTime').datetimepicker({
			timepicker : true,
			datepicker : false,
			format : 'H:i',
			hours12 : false,
			step : 15
		});

		$('#pickerDate').datetimepicker({
			timepicker : false,
			datepicker : true,
			format : 'd-m-Y',
			hours12 : false,
			step : 30,
			yearStart : 2019
		})
	</script>
	

</body>
</html>